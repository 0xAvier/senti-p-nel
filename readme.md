Senti(p)nel is a script that will tweet your local IP address.

If you follow this read.me, it will run every time your pi boots,
and wait for the internet connection to tweet.

# Installation process

Install npm:
```
apt-get install npm
```

Install is-online and twitter node packages.
```
npm install is-online
npm install twitter
```

Create a twitter app following the instruction there:
http://techknights.org/workshops/nodejs-twitterbot/ .

Stop before the `Hello World!` section.

Edit public_config.json: enter your credentials. 
Move public_config.json
```
mv public_config.json private_config.json
```

Install cron on volumio : 
```
apt-get install cron
```

Add an entry on the root crontab (see here: https://www.raspberrypi.org/documentation/linux/usage/cron.md):
```
sudo crontab -e
```
In our case, the entry is:
```
@reboot sleep 60 && `which node` /home/volumio/senti-p-nel/senti-p-nel.js 2>&1 /home/volumio/response &
```


The file `response` may be used to debug the script if there is an issue with
your twitter credentials.

Enjoy!
