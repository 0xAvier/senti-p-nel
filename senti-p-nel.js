
'use strict';

const isOnline = require('is-online');

isOnline().then(online => {
    var os = require('os');
    var ifaces = os.networkInterfaces();
    var TwitterPackage = require('twitter');
    var secret = require('./private_config.json');
    var Twitter = new TwitterPackage(secret);
    
    Object.keys(ifaces).forEach(function (ifname) {
      var alias = 0;
    
      ifaces[ifname].forEach(function (iface) {
        if ('IPv4' !== iface.family || iface.internal !== false) {
          // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
          return;
        }
    
        if (alias >= 1) {
          // this single interface has multiple ipv4 addresses
          console.log(ifname + ':' + alias, iface.address);
        } else {
          // this interface has only one ipv4 adress
          var datetime = new Date();
          Twitter.post('statuses/update', {status: datetime + '\n' + iface.address},  function(error, tweet, response){
              if(error){
                console.log(error);
              }
              console.log(tweet);  // Tweet body.
              console.log(response);  // Raw response object.
              process.exit(1)
          });
        }
        ++alias;
      });
    });
});


